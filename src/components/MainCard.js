import React from 'react';
import Card  from "../pages/Card";
import ReactDOM from 'react-dom';


const PostsData = [
  {
    "category": "News",
    "title": "Coffee Known as Living Savior",
    "text": "After recent studies, many researchers found that drinking coffee once a day...",
    "image": "https://source.unsplash.com/user/erondu/600x400"
  },
  {
    "category": "Travel",
    "title": "Greek Food Brings Astonishing Health Benefits",
    "text": "We've discovered the amazing secret ingrediants in Greek food that will...",
    "image": "https://source.unsplash.com/user/_vickyreyes/600x400"
  },
  {
    "category": "Desserts",
    "title": "This New Chooclate Lava Cake will have you begging for more",
    "text": "A small restaurant in Chicago has proved its...",
    "image": "https://source.unsplash.com/user/ilyapavlov/600x400"
  },
  {
    "category": "Dinner",
    "title": "This new Spanish chicken recipe is the newest craze!",
    "text": "Many families across the country are giving amazing ratings to this...",
    "image": "https://source.unsplash.com/user/erondu/600x400"
  },
]


class MainCard extends React.Component {
  constructor() {
    super();

    this.state = {
      posts: {}
    }
  }
  componentWillMount() {
    this.setState({
      posts: PostsData
    });
  }


  render() {
    return (
      <div>
      <header className="app-header" id="MainCard"></header>
      <div className="app-card-list" id="app-card-list">
        {
          Object
          .keys(this.state.posts)
          .map(key => <Card key={key} index={key} details={this.state.posts[key]}/>)
        }
    </div>
    </div>
    )
  }
}

export default MainCard
