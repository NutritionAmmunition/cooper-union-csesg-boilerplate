/*
   --------
   import the packages we need
   --------
 */

import React from 'react';
import { connect, Provider } from 'react-redux';
import { createStore, combineReducers, compose } from 'redux';
import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase';
import firebase from 'firebase';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { MuiThemeProvider } from '@material-ui/core/styles';
import theme from './style/theme';
import initialState from './initialState.json';
import './style/main.css';
import Header from './components/header';
import Grid from '@material-ui/core/Grid';
import ReactDOM from 'react-dom';
import Clarifai from 'clarifai';
import algoliasearch from 'algoliasearch';
 
/*
   --------
   import your pages here
   --------
 */

import HomePage from './pages/home';
import LoginPage from './pages/login';
import SignupPage from './pages/signup';
import SandwichesPage from './pages/sandwiches';
import CameraPage from './pages/camera';
import BlogPage from './pages/blog';
import ProfilePage from './pages/profile';
import Card from './pages/Card';
import QuestionPage from './pages/questionPage'
/*
   --------
   configure everything
   --------
 */

const firebaseConfig = {
    /*
       --------
       REPLACE WITH YOUR FIREBASE CREDENTIALS
       --------
     */
    apiKey: "AIzaSyAEY-OeysJz5rMhTXxDWaHKBavowja0rTo",
    authDomain: "nutrition-ammunition-23201.firebaseapp.com",
    databaseURL: "https://nutrition-ammunition-23201.firebaseio.com",
    projectId: "nutrition-ammunition-23201",
    storageBucket: "nutrition-ammunition-23201.appspot.com",
    messagingSenderId: "995529172708"
};

// react-redux-firebase config
const rrfConfig = {
    userProfile: 'users',
};

const app = new Clarifai.App({
 apiKey: 'b3e50471a90e474f8ffa313ad5921d9f'
});

const client = algoliasearch('RJS2Z7FT28', 'ccb3db1e3e4db044895fb1c394f1ca3e');
const index = client.initIndex('nutrition_ammunition');

// Initialize firebase instance
firebase.initializeApp(firebaseConfig);




/*
   --------
   setup redux and router
   --------
 */


const createStoreWithFirebase = compose(
    reactReduxFirebase(firebase, rrfConfig)
)(createStore);

// Add firebase to reducers
const rootReducer = combineReducers({
    firebase: firebaseReducer
});

const store = createStoreWithFirebase(rootReducer, initialState);


const ConnectedRouter = connect()(Router);



export default class App extends React.Component{
    render(){
	return(
	    <MuiThemeProvider theme={theme}>
		<Provider store={store}>
			<ConnectedRouter>
			    <div id="container">

					<Header></Header>
					<Route exact path="/" component={HomePage} />
					<Route exact path="/login" component={LoginPage} />
					<Route exact path="/signup" component={SignupPage} />
					<Route exact path="/sandwiches" component={SandwichesPage} />
          <Route exact path="/blog" component={BlogPage} />
          <Route exact path="/camera" component={CameraPage} />
          <Route exact path="/profile" component={ProfilePage} />
          <Route exact path="/Card" component={Card} />
          <Route exact path="/question" component={QuestionPage} />


			    </div>
			</ConnectedRouter>
		</Provider>
	    </MuiThemeProvider>
	);
    }
}
