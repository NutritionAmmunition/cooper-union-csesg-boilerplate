import React from 'react';

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import { connect } from "react-redux";
import { compose } from "redux";
import { Link } from "react-router-dom";
import { firebaseConnect, isLoaded, isEmpty } from "react-redux-firebase";
import * as Survey from 'survey-react';
import { Redirect } from "react-router";

const surveyJSON = {
 pages: [
  {
   name: "page1",
   elements: [
    {
     type: "radiogroup",
     name: "question1",
     title: "Are you vegan?",
     choices: [
      {
       value: "item1",
       text: "Yes"
      },
      {
       value: "item2",
       text: "No"
      }
     ]
    },
    {
     type: "radiogroup",
     name: "question5",
     title: "Are you vegetarian?",
     choices: [
      {
       value: "item2",
       text: "Yes"
      },
      {
       value: "item3",
       text: "No"
      }
     ]
    },
    {
     type: "radiogroup",
     name: "question2",
     title: "Are you gluten intolerant?",
     choices: [
      {
       value: "item1",
       text: "Yes"
      },
      {
       value: "item2",
       text: "No"
      }
     ]
    },
    {
     type: "radiogroup",
     name: "question3",
     title: "Are you lactose intolerant?",
     choices: [
      {
       value: "item1",
       text: "Yes"
      },
      {
       value: "item2",
       text: "No"
      }
     ]
    },
    {
     type: "checkbox",
     name: "question4",
     title: "Do you have any of these allergies?",
     choices: [
      {
       value: "item1",
       text: "Nuts"
      },
      {
       value: "item2",
       text: "Seafood"
      },
      {
       value: "item3",
       text: "Eggs"
      },
      {
       value: "item4",
       text: "Wheat"
      }
     ]
    }
   ]
  }
 ]
};



class QuestionPage extends React.Component{


  constructor(props){
    super(props);
    this.state = {
      done: false
    }
    this.sendDataToServer = this.sendDataToServer.bind(this);
  }

  sendDataToServer(survey) {
    let uid = this.props.auth.uid;
    this.props.firebase.update(`/users/${uid}/question`, survey.data)
      .then((response) => {
          this.setState({
            done: true
          });
      })
  }


  render(){
    let payload;
    if (!isLoaded(this.props.questionPage)) {
      payload = null;
    }
    if (isLoaded(this.props.questionPage) && !isEmpty(this.props.questionPage)) {
      payload = Object.keys(this.props.questionPage).map(key => {
        let questionPage = this.props.questionPage[key];
      });
    }
    if (this.state.done) {
      return(<Redirect to='camera' />);
    }
    return(
      <div>
      
      <Survey.Survey json={surveyJSON} onComplete={this.sendDataToServer}/>

      </div>
)
}
};

export default compose(
  firebaseConnect(props => [{ path: "questionsAnswer" }]),
  connect(({firebase: {auth}}) => ({auth})),
  connect((state, props) => ({
    questions: state.firebase.data.questions
  })),
  connect(({firebase: {profile}}) => ({profile}))
)(QuestionPage);
