import React from 'react';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { connect } from "react-redux";
import { compose } from "redux";
import { Link } from "react-router-dom";
import algoliasearch from 'algoliasearch';
import {
  InstantSearch,
  Hits,
  SearchBox,
  Highlight,
  RefinementList,
  Pagination,
  CurrentRefinements,
  ClearRefinements
} from 'react-instantsearch-dom';

//const Clarifai = require('clarifai');

function Search() {
  return (
    <div className="container">
      <CurrentRefinements />
      <ClearRefinements />
      <SearchBox />
      <RefinementList attribute="category" />
      <Hits hitComponent={Product} />
      <Pagination />
    </div>
  );
}

function Product({ hit }) {
  return (
    <div style={{ marginTop: '10px' }}>
      <span className="hit-name">
        <Highlight attribute="name" hit={hit} />
      </span>
    </div>
  );
}

const Clarifai = require('clarifai');

const clarifai = new Clarifai.App({
  apiKey: 'b7ce1cc2f63642df94db52c6a81d415f'
});

const client = algoliasearch('RJS2Z7FT28', 'ccb3db1e3e4db044895fb1c394f1ca3e');
const index = client.initIndex('nutrition_ammunition');

const buttonStyle =
{
   padding: '1em 2em',
   margin: '2em',
   fontFamily: 'Raleway',
   backgroundColor: '#cfcfd1',
   color: 'black',
   borderRadius: '2em',
}


class CameraPage extends React.Component{
  constructor(props){
	super(props);
	this.state = {
	    item: 0,
	   }
  }

  doAnalysis(){
    var result = clarifai.models.predict(Clarifai.GENERAL_MODEL, "https://images.buyagift.co.uk/common/client/Images/Product/Extralarge/en-GB/10737034-extralarge-1-new.jpg").then(
      function(response) {
        var foods = [];
        for (var i = 0; i < 9; i++)
        {
          foods[i] = (JSON.stringify(response.outputs[0].data.concepts[i].name));
          //console.log(JSON.stringify(response.outputs[0].data.concepts[i].name));
        }
        var listItems = foods.map((food) =>
         "<li>"+food+"</li>"
        );
        return listItems;
      },

      function(err) {
        console.log("It didn't work...");
      },
    );

    result.then((res) =>
      document.getElementById("result").innerHTML = res.join("\n")
    );

    // for (var key in res) {
    //   if (res.hasOwnProperty(key)) {
    //     console.log(key + " -> " + res[key]);
    //   }
    // }
    // console.log(res)
    // var resultDiv = document.getElementById("result");
    //resultDiv.innerHTML = res.join("\n");
  }

  render(){
    return(
      <div id="cameraPage">
      <Paper elevation={8} id="cameraPaper1">
      <ol>
      <li>Take a picture</li>
      <li>Upload it to your laptop</li>
      <li>Post onto this site</li>
      <li>Find out what ingrediants are in your meal</li>
      <li>Make the meal yourself!</li>
      </ol>
      </Paper>
      <br />
        <br />
          <br />
            <br />
              <br />
                <br />
	  <div>
      <Button variant="contained" color="inherit" style={buttonStyle} onClick={() => {this.doAnalysis();}}>Analyze Post</Button>
      <Button variant="contained" color="inherit" style={buttonStyle} onClick={() => {Search();}}>Search Recipes</Button>
      <div id="result"></div>
      <InstantSearch
        appId="RJS2Z7FT28"
        apiKey="ccb3db1e3e4db044895fb1c394f1ca3e"
        indexName="nutrition_ammunition"
      >
        <Search />
      </InstantSearch>
	  </div>
    </div>
)
}
};

export default CameraPage
