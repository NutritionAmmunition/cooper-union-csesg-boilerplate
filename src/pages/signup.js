import React from 'react';
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Paper from '@material-ui/core/Paper';
import { Link } from "react-router-dom";


class SignupPage extends React.Component{
    constructor(props){
	super(props);
	this.state = {
	    email: '',
	    password: ''
	};
    }

    handleChange(event, field){
	this.setState({
	    [field]: event.target.value
	});
    }

    signUserUp(event){
	event.preventDefault();
	this.props.firebase.createUser(this.state)
	    .then((response) => {
		// do something
	    })
	    .catch((error) => {
		switch(error.code){
		    case 'auth/email-already-in-use':
				alert("This email is already in use")
			break;
		    case 'auth/invalid-email':
		    	alert("This is an invalid email");
			break;
		    case 'auth/operation-not-allowed':
			// do something
			break;
		    case 'auth/weak-password':
				alert("Enter a stronger password");
			break;
		    default:
				"Oh No! Something went wrong :^)"
		}
	    });
    }

    render(){
	let payload;
	if(!this.props.auth.isLoaded){
	    // auth is not warmed up
	    payload = null;
	}
	if(this.props.auth.isLoaded && this.props.auth.isEmpty){
	    // auth is ready
	    // but user is not logged in
	    payload = <form onSubmit={(event) => {this.handleSubmit(event);}}>
	    <br />
	    <br />
	    <br />
        <Paper elevation={8} id="signupPaper">
		<FormControl fullWidth>
		    <TextField
			label="Email"
			value={this.state.email}
			onChange={(event) => {this.handleChange(event, 'email');}}
			margin="normal"
		    />
		</FormControl>
		<FormControl fullWidth>
		    <TextField
			label="Password"
			type="password"
			value={this.state.password}
			onChange={(event) => {this.handleChange(event, 'password');}}
			margin="normal"
		    />
		</FormControl>
    </Paper>
    <br />
    <br />
    <br />
    <Link to = "/signup">
		<Button type="submit"
			variant="contained"
			color="secondary"
			onClick={(event) => {this.signUserUp(event)}}> <font color="white">Signup</font></Button>
	</Link>
	    </form>;

	}
	if(this.props.auth.isLoaded && !this.props.auth.isEmpty){
	    // auth is warmed up
	    // and user is not logged in
	    payload = <div>
		<div>
		    Welcome {this.props.auth.email}
		</div>
		<div>
		    <Button variant="contained"
			    color="secondary"
			    onClick={() => {this.props.firebase.logout();}}>
			Logout
		    </Button>

		</div>
	    </div>;
	}
	return(
	    <div>
		{payload}
	    </div>
	)
    }
};

export default compose(
    firebaseConnect(),
    connect(({firebase: {auth}}) => ({auth}))
)(SignupPage);
