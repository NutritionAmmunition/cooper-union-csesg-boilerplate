import React from 'react';
import CardHeader from '../components/CardHeader';
import CardBody from '../components/CardBody';
import MainCard from '../components/MainCard';

class Card extends React.Component {
  render() {
    return (
      <article className="card">
        <CardHeader category={this.props.details.category} image={this.props.details.image}/>
        <CardBody title={this.props.details.title} text={this.props.details.text}/>
      </article>
    )
  }
}

export default Card
