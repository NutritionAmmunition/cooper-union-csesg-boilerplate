import React from 'react';
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Logo from './Logo.png';

class HomePage extends React.Component{
    render(){
    return(

        <div>
        <br/>
        <p id="words">
            Welcome!
        </p>
        <br/>
        <br/>
        <div className="bgimg-1"><br/><br/><br/><br/><br/>
        <br/><br/><br/><br/>
            <Paper id="GeneralPaper">
                <Link to = "/camera">
                        <Button className="buttons"id="homeButtons" variant="contained" color="secondary">
                        <font color="white"> Camera </font></Button>
                </Link>
                <br/><br/>
                <p id="words"> Explore the heart of our product through the interactive camera, which allows you to test NutritionAmmunition
                for yourself!</p>
            </Paper>
                <br/><br/><br/><br/>
            <Paper id="Paper2">
                <Link to = "/question">
                    <Button variant="contained" id="homeButtons" color="secondary"> 
                    <font color="white"> Prefrences </font></Button>
                </Link><br/><br/>
                <p id="words"> Set your food and meal prefrences while allowing our reactive UI to learn more about your dietary patterns and suggesting foods you 
                will probably love.</p>
            </Paper>
                <br/><br/><br/>
            <Paper id="Paper3">
                <Link to = "/blog">
                    <Button variant="contained" id="homeButtons" color="secondary">
                    <font color="white">Blog </font></Button>
                </Link><br/><br/>
                <p id="words">Learn what past users have said about our product and how they've used it in their lives.</p>
            </Paper>
        </div>

        <br/><br/><br/>
        <p id="words"> About Us </p>
        <br/><br/><br/>

        <div className="bgimg-2">
            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            <Paper elevation={10} id="GeneralPaper">
                <p>Nutrition Ammunition is a webapp designed to provide young adults with the resources and information needed for them to make their own meals based of their most loved restaurants. We aim to provide recipes and advice so that individuals are more aware of what they should eat and how to save money based on their lifestyle.  </p>
            </Paper>
        </div>

        <br/><br/><br/>
        <p id="words"> The App </p>
        <br/><br/><br/>

        <div className="bgimg-3">
            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            <Paper elevation={10} id="Paper2">
                <p> Our friendly app makes it fast and easy to cook the foods you love eating. All we need is a picture of the meal you want to cook and we will break down the ingredients, common recipes, and even healthier alternatives!</p>
            </Paper>
        </div>

        <br/><br/><br/>
        <p id="words"> Our Goal </p>
        <br/><br/><br/>

        <div className="bgimg-4">
            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            <Paper elevation={10} id="Paper3">
                <p> We aim to educate budding teenagers and young adults to learn what is in their food and how they can cook it.
                This can help them save money and learn the life-long valuable skill of cooking.</p>
            </Paper>
        </div>
        
        <br/>
        <br/>
        <br/>

        <p id="words"> NutritionAmmunition, it is our mission to power your kitchen! </p>

        <br/>
        <br/>
        <br/>

        <div className="bgimg-5">

        </div>

        </div>

    )
    }
};

export default HomePage